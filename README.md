# superHeroBack

Spring boot + json + rest </n>

Ejecucion Back
1.  clonar el proyecto
2.  actualizar dependencias
3.  Ejecutar como Spring Boot App

Ya se puede probar con peticiones http como por ejemplo postman

Ejecucion Front
1.  clonar el proyecto [WEB](https://gitlab.com/jochelo/superherofront)
2.  Ejecutar ng serve --o


Buscar datos segun la api https://superheroapi.com/


El servidor de la api tiene bloqueado CORS, por lo que no se puede llamar_
directamente desde componentes js, por lo que se ha creado el back, _
como puerta intermedia con CORS habilitado y asu vez este, _
invoca el servicio de la api