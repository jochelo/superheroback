package com.superheroes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SuperHeroesBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(SuperHeroesBackApplication.class, args);
	}

}
