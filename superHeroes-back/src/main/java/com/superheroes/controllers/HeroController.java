package com.superheroes.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.superheroes.entity.SuperHero;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class HeroController {

	@GetMapping("/searchSuperHero/{searched}")
	public ResponseEntity<?> show(@PathVariable String searched) {

		Map<String, Object> map = null;
		if(		searched.isEmpty()) {
			map = new HashMap<>();
			map.put("mensaje", "debe ingresar datos");
			return new ResponseEntity<Map<String, Object>>(map, HttpStatus.BAD_REQUEST);
		}
	
		final String url = "https://superheroapi.com/api/10218135692716651/search/".concat(searched);

		HttpResponse<JsonNode> response;
		try {
			response = Unirest.get(url).header("Content-Type", "text/plain").header("Accept", "*/*")
					.header("Cache-Control", "no-cache").header("Accept-Encoding", "gzip, deflate")
					.header("Referer", url).header("Connection", "keep-alive").header("cache-control", "no-cache")
					.asJson();

			JsonNode body = response.getBody();

			if (!body.getObject().isNull("error")) {
				map = new HashMap<>();
				map.put("mensaje", "No se encuentran coincidencias");
				return new ResponseEntity<Map<String, Object>>(map, HttpStatus.NOT_FOUND);
			}
				
			Object gObj = body.getObject().get("results");
			ObjectMapper mapper = new ObjectMapper();

			 CollectionType mapCollectionType = mapper.getTypeFactory().constructCollectionType(List.class,
					SuperHero.class);
			List<SuperHero> asList = mapper.readValue(gObj.toString(), mapCollectionType);

			return new ResponseEntity<List<SuperHero>>(asList, HttpStatus.OK);

		} catch (UnirestException e) {
			map = new HashMap<>();
			map.put("mensaje", "Error al hacer la llamada al servicio");
			map.put("error", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (JsonProcessingException e) {
			map = new HashMap<>();
			map.put("mensaje", "Error al pasear");
			map.put("error", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@GetMapping("/getSuperHeroById/{id}")
	public ResponseEntity<?> show(@PathVariable Long id) {

		final String url = "https://superheroapi.com/api/10218135692716651/".concat(String.valueOf(id));
		Map<String, Object> map = null;

		HttpResponse<JsonNode> response;
		try {
			response = Unirest.get(url).header("Content-Type", "text/plain").header("Accept", "*/*")
					.header("Cache-Control", "no-cache").header("Accept-Encoding", "gzip, deflate")
					.header("Referer", url).header("Connection", "keep-alive").header("cache-control", "no-cache")
					.asJson();

			JsonNode body = response.getBody();

			if (!body.getObject().isNull("error")) {
				map = new HashMap<>();
				map.put("mensaje", "No se encuentra coincidencia");
				return new ResponseEntity<Map<String, Object>>(map, HttpStatus.NOT_FOUND);
			}
				
			Object gObj = body.getObject();

			ObjectMapper mapper = new ObjectMapper();

			SuperHero asList = mapper.readValue(gObj.toString(), SuperHero.class);

			return new ResponseEntity<SuperHero>(asList, HttpStatus.OK);

		} catch (UnirestException e) {
			map = new HashMap<>();
			map.put("mensaje", "Error al hacer la llamada al servicio");
			map.put("error", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (JsonProcessingException e) {
			map = new HashMap<>();
			map.put("mensaje", "Error al pasear");
			map.put("error", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}
