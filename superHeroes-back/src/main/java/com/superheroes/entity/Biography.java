package com.superheroes.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Biography implements Serializable {

	private static final long serialVersionUID = 9080177530820838526L;

	private String fullName;
	private String birthPlace;
	private String firstAppearance;
	private String publisher;
	private String alignment;

	@JsonProperty("fullName")
	public String getFullName() {
		return fullName;
	}


	@JsonProperty("full-name")
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@JsonProperty("birthPlace")
	public String getBirthPlace() {
		return birthPlace;
	}

	@JsonProperty("place-of-birth")
	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}

	@JsonProperty("firstAppearance")
	public String getFirstAppearance() {
		return firstAppearance;
	}

	@JsonProperty("first-appearance")
	public void setFirstAppearance(String firstAppearance) {
		this.firstAppearance = firstAppearance;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getAlignment() {
		return alignment;
	}

	public void setAlignment(String alignment) {
		this.alignment = alignment;
	}

	@Override
	public String toString() {
		return "Biography [fullName=" + fullName + ", birthPlace=" + birthPlace + ", firstAppearance=" + firstAppearance
				+ ", publisher=" + publisher + ", alignment=" + alignment + "]";
	}

}
