package com.superheroes.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SuperHero implements Serializable {

	private static final long serialVersionUID = -8557481555881706290L;
	private Long id;
	private String name;
	private Biography biography;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Biography getBiography() {
		return biography;
	}
	public void setBiography(Biography biography) {
		this.biography = biography;
	}
	@Override
	public String toString() {
		return "SuperHero [id=" + id + ", name=" + name + ", biography=" + biography + "]";
	}
}
